#! /usr/bin/env python

from distutils.core import setup, Extension
from spritefile import version

try:

    from Pyrex.Distutils import build_ext
    use_pyrex = 1

except ImportError:

    use_pyrex = 0

# Compile spriteops.pyx with pyrexc.
#
#print "Compiling spriteops.pyx..."
#os.system("pyrexc spriteops.pyx")

if use_pyrex == 1:

    setup(
        name="spritefile",
        description="A module for reading and writing Acorn Spritefiles.",
        author="David Boddie",
        author_email="david@boddie.org.uk",
        url="http://www.boddie.org.uk/david/Projects/Python/Spritefile/",
        version=version,
        py_modules=["spritefile"],
        ext_modules=[Extension("spriteops", ["spriteops.pyx"])],
        cmdclass = {'build_ext': build_ext},
        scripts=["Tools/spr2img.py", "Tools/spr2other.py", "Tools/pygimp-sprite.py"]
        )

else:

    setup(
        name="spritefile",
        description="A module for reading and writing Acorn Spritefiles.",
        author="David Boddie",
        author_email="david@boddie.org.uk",
        url="http://www.boddie.org.uk/david/Projects/Python/Spritefile/",
        version=version,
        py_modules=["spritefile"],
        scripts=["Tools/spr2img.py", "Tools/spr2other.py", "Tools/pygimp-sprite.py"]
        )
