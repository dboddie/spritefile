#!/usr/bin/env python

"""
spr2other.py

Convert individual sprites stored in Acorn Spritefiles into other formats
using the Python Imaging Library.

(C) Paul Boddie 2003-2005

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.
"""

if __name__ == "__main__":

    # Import the spritefile module
    import spritefile
    
    # Ensure the presence of PIL
    try:
        import PIL.Image as Image
    except ImportError:
        try:
            import Image
        except ImportError:
            print "Python Imaging Library not found as PIL.Image or Image."
            sys.exit(1)

    # Perform a conversion according to the command line arguments
    import sys

    try:
        in_file, format, out_file, sprite_name = sys.argv[1:5]
    except ValueError:
        print "Please specify input filename, output format, output filename,"
        print "and the name of the sprite to be converted (or _ for the only"
        print "sprite in a single image spritefile)."
        print
        print "For example:"
        print
        print "python spritefile.py Building,ff9 jpeg Building.jpg building"
        sys.exit(1)

    print "Using format", format

    # Open the Sprite file
    sprite = spritefile.spritefile(open(in_file, "rb"))

    # Find the image to be converted
    if sprite_name == "_" and len(sprite.sprites.keys()) == 1:
        image = sprite.sprites[sprite.sprites.keys()[0]]
    elif sprite.sprites.has_key(sprite_name):
        image = sprite.sprites[sprite_name]
    else:
        print "Please specify a valid sprite name from the following list:"
        print " ".join(sprite.sprites.keys())
        sys.exit(1)

    # Using PIL, write the data to the output file
    out_image = Image.fromstring(image['mode'], (image['width'], image['height']), image['image'])
    out_image.save(open(out_file, "wb"), format)
