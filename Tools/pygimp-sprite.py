#!/usr/bin/env python

"""
Sprite format plug-in for The Gimp.

(C) 2003, Paul Boddie, David Boddie

First, install spritefile using the supplied distutils script.
Then, copy this file into the ~/.gimp-1.2/plug-ins directory.
(The permissions might need to be set to executable on this file in that
directory.)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.
"""

import spritefile
from gimpfu import *

def python_sprite_open(filename, *args):

    """
    Open the sprite file with the given 'filename', returning images for all
    sprite images found in the file.
    """

    gimp.progress_init("Opening Sprite file...")
    gimp.progress_update(0)
    sprite = spritefile.spritefile(open(filename, "rb"))
    gimp.progress_update(100)

    # Initialise the resulting image list and progress information.

    images = []
    number_of_images = len(sprite.sprites.keys())
    number_of_images_read = 0

    # Show the progress at the image level.

    gimp.progress_init("Creating images...")
    gimp.progress_update(0)

    for sprite_name in sprite.sprites.keys():
        sprite_image = sprite.sprites[sprite_name]

        # Determine the mode constant.

        if sprite_image["mode"] == "RGB":
            mode = RGB_IMAGE
        elif sprite_image["mode"] == "RGBA":
            mode = RGBA_IMAGE
        elif sprite_image["mode"] == "CMYK":
        
            # Conversion from RGB to CMYK should occur automatically
            # in the spritefile object.
            mode = RGB_IMAGE
        
        else:

            # NOTE: Other modes not directly supported.
            continue
        
        # Create an RGB image. Alpha channels, if necessary, will be dealt
        # with later.
        image = gimp.image(sprite_image["width"], sprite_image["height"], RGB_IMAGE)
        
        # Create a layer which will support an alpha channel, if required.
        drawable = gimp.layer(image, "Sprite image data", sprite_image["width"],
            sprite_image["height"], mode, 100, NORMAL_MODE)
        
        # Add the layer to the image.
        layer = image.add_layer(drawable, 0)
        region = drawable.get_pixel_rgn(0, 0, sprite_image["width"], sprite_image["height"])

        # Now copy the data into the image.

        i = 0

        # Show the progress at the row level.

        gimp.progress_init("Loading image data...")
        gimp.progress_update(0)
        
        if mode == RGB_IMAGE:
        
            for y in xrange(0, sprite_image["height"]):
            
                # Get the RGB values and set them in the region.
                
                rgb = sprite_image["image"][i:i+sprite_image["width"]*3]
                region[0:sprite_image["width"], y] = rgb
                i += sprite_image["width"] * 3
                
                # Update the progress at the image level.
                
                gimp.progress_update((y + 1) * 100.0 / sprite_image["height"])
        
        elif mode == RGBA_IMAGE:
        
            # Add an alpha channel to the layer.
            drawable.add_alpha()
            
            for y in xrange(0, sprite_image["height"]):
            
                # Get the RGBA values and set them in the region.
                
                rgba = sprite_image["image"][i:i+sprite_image["width"]*4]
                
                region[0:sprite_image["width"], y] = rgba
                i += sprite_image["width"] * 4
                
                # Update the progress at the image level.
                
                gimp.progress_update((y + 1) * 100.0 / sprite_image["height"])
        
        # Display the image in its own window.

        images.append(gimp.display(image))

        # Update the progress meter.

        number_of_images_read += 1
        gimp.progress_init("Creating images...")
        gimp.progress_update(number_of_images_read * 100.0 / number_of_images)

register(
    "python_sprite_open",
    "Open Acorn Sprite file format images",
    "Open Acorn Sprite file format images using the spritefile module",
    "Paul Boddie, David Boddie",
    "Paul Boddie, David Boddie",
    "2003",
    "<Load>/Sprite",
    None, # Should mean "open in all conditions".
    [
        (PF_FILE, "filename", "Sprite filename", ""),
        (PF_FILE, "raw_filename", "Sprite filename", ""),
    ],
    [ # No formal results.
        (PF_IMAGE, "image", "The loaded image"),
    ],
    python_sprite_open)

main()

# vim: tabstop=4 expandtab shiftwidth=4
