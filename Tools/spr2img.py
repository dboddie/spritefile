#!/usr/bin/env python
"""
spr2img.py

Convert Acorn Spritefiles into other formats using the Python Imaging Library.
Spritefiles containing multiple images are split into many files which can be
stored in a named directory.

(C) David Boddie 2003-2005

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.
"""

import Image, os, spritefile, sys


def list_sprites(sf, spr):

    # Print information on the sprites.
    print
    print 'Spritefile "%s" contains:' % sf
    print
    
    if len(spr.sprites) > 0:
    
        for name in spr.sprites.keys():
        
            print name
        
        print
    
    return


def convert_sprites(spr, output_dir, format):

    # Convert each sprite to the format which uses the suffix given.
    for name, sprite in spr.sprites.items():
    
        # Create an Image object.
        image = Image.fromstring(
            sprite['mode'], (sprite['width'], sprite['height']),
            sprite['image']
            )
        
        # Write the image to a file in the output directory.
        try:
        
            path = os.path.join(output_dir, name) + suffix_sep + format
            fp = open(path, "wb")
            
            image.save(fp, format)
            fp.close()
        
        except IOError:
        
            print "Failed to open file for sprite: %s" % path
        
        except KeyError:
        
            print "Could not convert sprite to format: %s" % format
            fp.close()
            os.remove(path)
            break
    
    return


def usage():

    sys.stderr.write("Usage: %s --list <spritefile>\n" % sys.argv[0])
    sys.stderr.write("Usage: %s [format] <spritefile> <output directory>\n\n" % sys.argv[0])
    sys.stderr.write(
        "List the sprites contained in a Spritefile or extract them to a directory,\n"
        "converting them to another format in the process. The output format is\n"
        'determined by the string specified; the default format is "png" (PNG format).\n'
        )
    sys.exit()


if __name__ == "__main__":

    args = sys.argv[1:]
    listing = False
    format = "png"
    
    if len(args) < 2:
        usage()
    
    elif len(args) == 2 and args[0] == "--list":
        listing = True
        sf = args[1]
    
    elif 2 <= len(args) <= 3:
        output_dir = args.pop()
        sf = args.pop()
        if args: format = args.pop()
    
    else:
        usage()
    
    if not listing:
    
        # Define a suffix separator.
        if sys.platform == "RISCOS":
            suffix_sep = "/"
        else:
            suffix_sep = "."
        
        # Try to create the directory.
        if os.path.exists(output_dir):
            if not os.path.isdir(output_dir):
                sys.stderr.write("Could not create directory; a file already "
                    "exists: %s\n" % output_dir)
                sys.exit()
        else:
            try:
                os.mkdir(output_dir)
            except OSError:
                sys.stderr.write("Could not create directory: %s\n" % output_dir)
                sys.exit()
    
    # Open the spritefile.
    try:
        spr = spritefile.spritefile(open(sf, "rb"))
    
    except IOError:
        sys.stderr.write("File could not be read: %s\n" % sf)
        sys.exit()
    
    if listing:
        list_sprites(sf, spr)
    else:
        convert_sprites(spr, output_dir, format)
    
    sys.exit()
