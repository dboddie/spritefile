Spritefile
==========

The spritefile module provides a class for reading and writing Acorn
Spritefiles, converting the images contained within into a format suitable
for use with the Python Imaging Library:

    http://www.pythonware.com/products/pil/

The module is written in a combination of Python and Pyrex, which allows a
faster conversion of image data than a purely interpreted module, but will
fall back on a pure Python implementation if the Pyrex functions could not
be compiled.

A number of tools providing front ends to the conversion facilities to the
module are provided in the Tools directory. These include command line
conversion tools which use the Python Imaging Library for their output and
a loader plugin (courtesy of Paul Boddie) for The GIMP; the plugin requires
the pygimp package:

    http://www.daa.com.au/~james/software/pygimp/
